require 'test_helper'

class ResourceControllerTest < ActionController::TestCase
  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get summary" do
    get :summary
    assert_response :success
  end

end

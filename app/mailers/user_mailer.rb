class UserMailer < ActionMailer::Base

  def account_activation(user)
    mg_client = Mailgun::Client.new "key-c3999ff838bae374087bfae3dc73ee61"

    @user = user
    email_with_name = "#{@user.name} <#{@user.email}>"

    message_params = {
      from:     'cummins@brijex.com',
      to:       email_with_name,
      subject:  'Account Activation.',
      html:     (render_to_string template: 'user_mailer/account_activation' ).to_str
    }

    mg_client.send_message "brijex.com", message_params
  end

  def acceptance_email(user)
    mg_client = Mailgun::Client.new "key-c3999ff838bae374087bfae3dc73ee61"

    @user = user
    email_with_name = "#{@user.name} <#{@user.email}>"

    message_params = {
      from:     'cummins@brijex.com',
      to:       email_with_name,
      subject:  'Proyecto Aceptado',
      html:     (render_to_string template: 'user_mailer/acceptance_email' ).to_str
    }

    mg_client.send_message "brijex.com", message_params
  end

  def rejection_email(user)
    mg_client = Mailgun::Client.new "key-c3999ff838bae374087bfae3dc73ee61"

    @user = user
    email_with_name = "#{@user.name} <#{@user.email}>"

    message_params = {
      from:     'cummins@brijex.com',
      to:       email_with_name,
      subject:  'Proyecto Rechazado',
      html:     (render_to_string template: 'user_mailer/rejection_email' ).to_str
    }

    mg_client.send_message "brijex.com", message_params
  end
end

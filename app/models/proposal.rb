# == Schema Information
#
# Table name: proposals
#
#  id                      :integer          not null, primary key
#  action_1a               :string(255)
#  action_1b               :string(255)
#  action_1c               :string(255)
#  action_2a               :string(255)
#  action_2b               :string(255)
#  action_2c               :string(255)
#  action_3a               :string(255)
#  action_3b               :string(255)
#  action_3c               :string(255)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  user_id                 :integer
#  methodology_process     :string(600)
#  methodology_medium      :string(600)
#  methodology_description :string(600)
#  impact_objective1       :string(255)
#  impact_objective2       :string(255)
#  impact_objective3       :string(255)
#  impact_goal1            :string(255)
#  impact_goal2            :string(255)
#  impact_goal3            :string(255)
#  impact_indicator1       :string(255)
#  impact_indicator2       :string(255)
#  impact_indicator3       :string(255)
#  impact_evidence1        :string(255)
#  impact_evidence2        :string(255)
#  impact_evidence3        :string(255)
#

class Proposal < ActiveRecord::Base
	belongs_to :user
	has_one :section
	has_one :resource

	before_create :create_section

	def create_section
    	build_section
    	true
  	end
end

ready = ->
  
  sum = $('#proyect_investment_sum')
  alerta = $('#alerta')
  b = $('#proyect_investment_b')
  c = $('#proyect_investment_c')
  d = $('#proyect_investment_d')
  e = $('#proyect_investment_e')
  f = $('#proyect_investment_f')

  sumar = ->
    value = parseInt(b.val()) + parseInt(c.val()) + parseInt(d.val()) + parseInt(e.val()) + parseInt(f.val())
    sum.val(value)
    if (value > 300000)
      alerta.show(100)
    else 
      alerta.hide(100)

  b.change ->
    sumar()
  c.change ->
    sumar()
  d.change ->
    sumar()
  e.change ->
    sumar()
  f.change ->
    sumar()

  b.keyup ->
    sumar()
  c.keyup ->
    sumar()
  d.keyup ->
    sumar()
  e.keyup ->
    sumar()
  f.keyup ->
    sumar()

    

  
$(document).ready(ready)
$(document).on('page:load', ready)
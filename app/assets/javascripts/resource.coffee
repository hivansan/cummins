ready = ->
  accion_1_1 = {
    a1 : $('#resource_selection_cost_action1_1a'),
    a2 : $('#resource_selection_cost_action2_1a'),
    a3 : $('#resource_selection_cost_action3_1a'),
    a4 : $('#resource_selection_cost_action4_1a'),
    a5 : $('#resource_selection_cost_action5_1a')
  }

  accion_1_2 = {
    a1 : $('#resource_selection_cost_action1_1b'),
    a2 : $('#resource_selection_cost_action2_1b'),
    a3 : $('#resource_selection_cost_action3_1b'),
    a4 : $('#resource_selection_cost_action4_1b'),
    a5 : $('#resource_selection_cost_action5_1b')
  }

  accion_1_3 = {
    a1 : $('#resource_selection_cost_action1_1c'),
    a2 : $('#resource_selection_cost_action2_1c'),
    a3 : $('#resource_selection_cost_action3_1c'),
    a4 : $('#resource_selection_cost_action4_1c'),
    a5 : $('#resource_selection_cost_action5_1c')
  }
  
  accion_2_1 = {
    a1 : $('#resource_selection_cost_action1_2a'),
    a2 : $('#resource_selection_cost_action2_2a'),
    a3 : $('#resource_selection_cost_action3_2a'),
    a4 : $('#resource_selection_cost_action4_2a'),
    a5 : $('#resource_selection_cost_action5_2a')
  }

  accion_2_2 = {
    a1 : $('#resource_selection_cost_action1_2b'),
    a2 : $('#resource_selection_cost_action2_2b'),
    a3 : $('#resource_selection_cost_action3_2b'),
    a4 : $('#resource_selection_cost_action4_2b'),
    a5 : $('#resource_selection_cost_action5_2b')
  }

  accion_2_3 = {
    a1 : $('#resource_selection_cost_action1_2c'),
    a2 : $('#resource_selection_cost_action2_2c'),
    a3 : $('#resource_selection_cost_action3_2c'),
    a4 : $('#resource_selection_cost_action4_2c'),
    a5 : $('#resource_selection_cost_action5_2c')
  }

  accion_3_1 = {
    a1 : $('#resource_selection_cost_action1_3a'),
    a2 : $('#resource_selection_cost_action2_3a'),
    a3 : $('#resource_selection_cost_action3_3a'),
    a4 : $('#resource_selection_cost_action4_3a'),
    a5 : $('#resource_selection_cost_action5_3a')
  }

  accion_3_2 = {
    a1 : $('#resource_selection_cost_action1_3b'),
    a2 : $('#resource_selection_cost_action2_3b'),
    a3 : $('#resource_selection_cost_action3_3b'),
    a4 : $('#resource_selection_cost_action4_3b'),
    a5 : $('#resource_selection_cost_action5_3b')
  }

  accion_3_3 = {
    a1 : $('#resource_selection_cost_action1_3c'),
    a2 : $('#resource_selection_cost_action2_3c'),
    a3 : $('#resource_selection_cost_action3_3c'),
    a4 : $('#resource_selection_cost_action4_3c'),
    a5 : $('#resource_selection_cost_action5_3c')
  }
  
  tot_a1_1 = $('#resource_total_cost_action_1a')
  tot_a1_2 = $('#resource_total_cost_action_1b')
  tot_a1_3 = $('#resource_total_cost_action_1c')

  tot_a2_1 = $('#resource_total_cost_action_2a')
  tot_a2_2 = $('#resource_total_cost_action_2b')
  tot_a2_3 = $('#resource_total_cost_action_2c')

  tot_a3_1 = $('#resource_total_cost_action_3a')
  tot_a3_2 = $('#resource_total_cost_action_3b')
  tot_a3_3 = $('#resource_total_cost_action_3c')

  $("[id^=resource_selection_cost_action]").change -> 
    a_1_1 = 0
    for k,v of accion_1_1
      if v.val() != ""
        a_1_1 += parseInt(v.val())
    tot_a1_1.val(a_1_1)

    a_1_2 = 0
    for k,v of accion_1_2
      if v.val() != ""
        a_1_2 += parseInt(v.val())
    tot_a1_2.val(a_1_2)

    a_1_3 = 0
    for k,v of accion_1_3
      if v.val() != ""
        a_1_3 += parseInt(v.val())
    tot_a1_3.val(a_1_3)

    a_2_1 = 0
    for k,v of accion_2_1
      if v.val() != ""
        a_2_1 += parseInt(v.val())
    tot_a2_1.val(a_2_1)

    a_2_2 = 0
    for k,v of accion_2_2
      if v.val() != ""
        a_2_2 += parseInt(v.val())
    tot_a2_2.val(a_2_2)

    a_2_3 = 0
    for k,v of accion_2_3
      if v.val() != ""
        a_2_3 += parseInt(v.val())
    tot_a2_3.val(a_2_3)

    a_3_1 = 0
    for k,v of accion_3_1
      if v.val() != ""
        a_3_1 += parseInt(v.val())
    tot_a3_1.val(a_3_1)

    a_3_2 = 0
    for k,v of accion_3_2
      if v.val() != ""
        a_3_2 += parseInt(v.val())
    tot_a3_2.val(a_3_2)

    a_3_3 = 0
    for k,v of accion_3_3
      if v.val() != ""
        a_3_3 += parseInt(v.val())
    tot_a3_3.val(a_3_3)


$(document).ready(ready)
$(document).on('page:load', ready)
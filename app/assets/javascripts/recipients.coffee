ready = ->
  beneficiarios = gon.beneficiarios
  eventuales = $('#proyect_ben_part_eventual')  
  regulares = $('#proyect_ben_part_regular')  
  alerta = $('#alerta')  

  eventuales.change ->
    regulares.attr("max", beneficiarios - parseInt(eventuales.val()))
    if (parseInt(eventuales.val()) + parseInt(regulares.val()) > beneficiarios)
      alerta.show(100)
      # regulares.attr("max", beneficiarios - parseInt(eventuales.val()))
    else 
      alerta.hide(100)


  regulares.change ->
    eventuales.attr("max", beneficiarios - parseInt(regulares.val()))
    if (parseInt(eventuales.val()) + parseInt(regulares.val()) > beneficiarios)
      alerta.show(100)
    else 
      alerta.hide(100)


$(document).ready(ready)
$(document).on('page:load', ready)
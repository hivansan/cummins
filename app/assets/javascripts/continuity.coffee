ready = ->

  a2a = $('#action_2a')
  val_a2a = $('#action_2a input[type=text]').val()
  btn_a2a = $('#show_a2a')

  a3a = $('#action_3a')
  val_a3a = $('#action_3a input[type=text]').val()
  btn_a3a = $('#show_a3a')


  btn_a2a.click ->
    a2a.show(100)
    $(this).hide(100)

  btn_a3a.click ->
    a3a.show(100)
    $(this).hide(100)

  if val_a2a == ""
    a2a.hide() # works
  else
    btn_a2a.hide()

  if val_a3a == ""
    a3a.hide() # works
  else
    btn_a3a.hide()


  a2b = $('#action_2b')
  val_a2b = $('#action_2b input[type=text]').val()
  btn_a2b = $('#show_a2b')

  a3b = $('#action_3b')
  val_a3b = $('#action_3b input[type=text]').val()
  btn_a3b = $('#show_a3b')


  btn_a2b.click ->
    a2b.show(100)
    $(this).hide(100)

  btn_a3b.click ->
    a3b.show(100)
    $(this).hide(100)

  if val_a2b == ""
    a2b.hide() # works
  else
    btn_a2b.hide()

  if val_a3b == ""
    a3b.hide() # works
  else
    btn_a3b.hide()


  # -------------------------------------------
  a2c = $('#action_2c')
  val_a2c = $('#action_2c input[type=text]').val()
  btn_a2c = $('#show_a2c')

  a3c = $('#action_3c')
  val_a3c = $('#action_3c input[type=text]').val()
  btn_a3c = $('#show_a3c')


  btn_a2c.click ->
    a2c.show(100)
    $(this).hide(100)

  btn_a3c.click ->
    a3c.show(100)
    $(this).hide(100)

  if val_a2c == ""
    a2c.hide() # works
  else
    btn_a2c.hide()

  if val_a3c == ""
    a3c.hide() # works
  else
    btn_a3c.hide()
  
  
$(document).ready(ready)
$(document).on('page:load', ready)
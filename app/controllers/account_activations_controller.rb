class AccountActivationsController < ApplicationController
  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.activate
      log_in user 
      flash[:success] = "Cuenta Activada!"
      redirect_to edit_proyect_path(user.proyect)
    else
      gon.notification = {
        class: "danger", 
        message: "Link de activacion invalido"
      }
      flash[:danger] = "Link de activacion invalido"
      redirect_to root_url
    end
  end
end
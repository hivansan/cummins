class DefaultsJustification < ActiveRecord::Migration
  def change
    change_column :proyects, :importance_text, :string, default: ""
    change_column :proyects, :importance_text_b, :string, default: ""
    change_column :proyects, :importance_text_c, :string, default: ""
    change_column :proyects, :importance_text_d, :string, default: ""
    change_column :proyects, :importance_text_f, :string, default: ""
    change_column :proyects, :importance_text_g, :string, default: ""
    change_column :proyects, :importance_text_h, :string, default: ""

  end
end

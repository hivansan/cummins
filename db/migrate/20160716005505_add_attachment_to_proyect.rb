class AddAttachmentToProyect < ActiveRecord::Migration
  def change
    add_column :proyects, :pfile_file_name, :string
    add_column :proyects, :pfile_content_type, :string
    add_column :proyects, :pfile_file_size, :integer
    add_column :proyects, :pfile_updated_at, :timestamp
  end
end

class AddActions < ActiveRecord::Migration
  def change
    add_column :proyects, :col_continuity_action1_b, :string, default: ""
    add_column :proyects, :col_continuity_action2_b, :string, default: ""
    add_column :proyects, :col_continuity_action3_b, :string, default: ""

    add_column :proyects, :col_continuity_action1_c, :string, default: ""
    add_column :proyects, :col_continuity_action2_c, :string, default: ""
    add_column :proyects, :col_continuity_action3_c, :string, default: ""

  end
end

class AddColumnToResources < ActiveRecord::Migration
  def change
    add_column :resources, :week_init_action_1a, :integer
    add_column :resources, :week_init_action_1b, :integer
    add_column :resources, :week_init_action_1c, :integer
    add_column :resources, :week_init_action_2a, :integer
    add_column :resources, :week_init_action_2b, :integer
    add_column :resources, :week_init_action_2c, :integer
    add_column :resources, :week_init_action_3a, :integer
    add_column :resources, :week_init_action_3b, :integer
    add_column :resources, :week_init_action_3c, :integer

    add_column :resources, :week_fin_action_1a, :integer
    add_column :resources, :week_fin_action_1b, :integer
    add_column :resources, :week_fin_action_1c, :integer
    add_column :resources, :week_fin_action_2a, :integer
    add_column :resources, :week_fin_action_2b, :integer
    add_column :resources, :week_fin_action_2c, :integer
    add_column :resources, :week_fin_action_3a, :integer
    add_column :resources, :week_fin_action_3b, :integer
    add_column :resources, :week_fin_action_3c, :integer
  end
end

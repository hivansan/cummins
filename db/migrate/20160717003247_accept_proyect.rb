class AcceptProyect < ActiveRecord::Migration
  def change
    add_column :proyects, :accepted, :integer, default: 0
  end
end

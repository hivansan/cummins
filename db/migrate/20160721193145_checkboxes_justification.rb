class CheckboxesJustification < ActiveRecord::Migration
  def change
    add_column :proyects, :idea_emerge_a, :integer, default: 0
    add_column :proyects, :idea_emerge_b, :integer, default: 0
    add_column :proyects, :idea_emerge_c, :integer, default: 0
    add_column :proyects, :procedure_taken_exp, :string, default: ""
  end
end

class Addzipproyect < ActiveRecord::Migration
  def change
    add_column :proyects, :zipfile_file_name, :string
    add_column :proyects, :zipfile_content_type, :string
    add_column :proyects, :zipfile_file_size, :integer
    add_column :proyects, :zipfile_updated_at, :timestamp
  end
end

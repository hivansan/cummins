class CreateResources < ActiveRecord::Migration
  def change
    create_table :resources do |t|
      t.string :concept1_action_1a, :limit => 30
      t.string :concept2_action_1a, :limit => 30
      t.string :concept3_action_1a, :limit => 30
      t.string :concept4_action_1a, :limit => 30
      t.string :concept5_action_1a, :limit => 30
      t.string :concept1_action_1b, :limit => 30
      t.string :concept2_action_1b, :limit => 30
      t.string :concept3_action_1b, :limit => 30
      t.string :concept4_action_1b, :limit => 30
      t.string :concept5_action_1b, :limit => 30
      t.string :concept1_action_1c, :limit => 30
      t.string :concept2_action_1c, :limit => 30
      t.string :concept3_action_1c, :limit => 30
      t.string :concept4_action_1c, :limit => 30
      t.string :concept5_action_1c, :limit => 30
      t.string :concept1_action_2a, :limit => 30
      t.string :concept2_action_2a, :limit => 30
      t.string :concept3_action_2a, :limit => 30
      t.string :concept4_action_2a, :limit => 30
      t.string :concept5_action_2a, :limit => 30
      t.string :concept1_action_2b, :limit => 30
      t.string :concept2_action_2b, :limit => 30
      t.string :concept3_action_2b, :limit => 30
      t.string :concept4_action_2b, :limit => 30
      t.string :concept5_action_2b, :limit => 30
      t.string :concept1_action_2c, :limit => 30
      t.string :concept2_action_2c, :limit => 30
      t.string :concept3_action_2c, :limit => 30
      t.string :concept4_action_2c, :limit => 30
      t.string :concept5_action_2c, :limit => 30
      t.string :concept1_action_3a, :limit => 30
      t.string :concept2_action_3a, :limit => 30
      t.string :concept3_action_3a, :limit => 30
      t.string :concept4_action_3a, :limit => 30
      t.string :concept5_action_3a, :limit => 30
      t.string :concept1_action_3b, :limit => 30
      t.string :concept2_action_3b, :limit => 30
      t.string :concept3_action_3b, :limit => 30
      t.string :concept4_action_3b, :limit => 30
      t.string :concept5_action_3b, :limit => 30
      t.string :concept1_action_3c, :limit => 30
      t.string :concept2_action_3c, :limit => 30
      t.string :concept3_action_3c, :limit => 30
      t.string :concept4_action_3c, :limit => 30
      t.string :concept5_action_3c, :limit => 30

      t.string :clasif1_action_1a, :limit => 30
      t.string :clasif2_action_1a, :limit => 30
      t.string :clasif3_action_1a, :limit => 30
      t.string :clasif4_action_1a, :limit => 30
      t.string :clasif5_action_1a, :limit => 30
      t.string :clasif1_action_1b, :limit => 30
      t.string :clasif2_action_1b, :limit => 30
      t.string :clasif3_action_1b, :limit => 30
      t.string :clasif4_action_1b, :limit => 30
      t.string :clasif5_action_1b, :limit => 30
      t.string :clasif1_action_1c, :limit => 30
      t.string :clasif2_action_1c, :limit => 30
      t.string :clasif3_action_1c, :limit => 30
      t.string :clasif4_action_1c, :limit => 30
      t.string :clasif5_action_1c, :limit => 30
      t.string :clasif1_action_2a, :limit => 30
      t.string :clasif2_action_2a, :limit => 30
      t.string :clasif3_action_2a, :limit => 30
      t.string :clasif4_action_2a, :limit => 30
      t.string :clasif5_action_2a, :limit => 30
      t.string :clasif1_action_2b, :limit => 30
      t.string :clasif2_action_2b, :limit => 30
      t.string :clasif3_action_2b, :limit => 30
      t.string :clasif4_action_2b, :limit => 30
      t.string :clasif5_action_2b, :limit => 30
      t.string :clasif1_action_2c, :limit => 30
      t.string :clasif2_action_2c, :limit => 30
      t.string :clasif3_action_2c, :limit => 30
      t.string :clasif4_action_2c, :limit => 30
      t.string :clasif5_action_2c, :limit => 30
      t.string :clasif1_action_3a, :limit => 30
      t.string :clasif2_action_3a, :limit => 30
      t.string :clasif3_action_3a, :limit => 30
      t.string :clasif4_action_3a, :limit => 30
      t.string :clasif5_action_3a, :limit => 30
      t.string :clasif1_action_3b, :limit => 30
      t.string :clasif2_action_3b, :limit => 30
      t.string :clasif3_action_3b, :limit => 30
      t.string :clasif4_action_3b, :limit => 30
      t.string :clasif5_action_3b, :limit => 30
      t.string :clasif1_action_3c, :limit => 30
      t.string :clasif2_action_3c, :limit => 30
      t.string :clasif3_action_3c, :limit => 30
      t.string :clasif4_action_3c, :limit => 30
      t.string :clasif5_action_3c, :limit => 30

      t.string :name1_prov1_action_1a, :limit => 30
      t.string :name1_prov2_action_1a, :limit => 30
      t.string :name1_prov3_action_1a, :limit => 30
      t.string :name1_prov4_action_1a, :limit => 30
      t.string :name1_prov5_action_1a, :limit => 30
      t.string :name1_prov1_action_1b, :limit => 30
      t.string :name1_prov2_action_1b, :limit => 30
      t.string :name1_prov3_action_1b, :limit => 30
      t.string :name1_prov4_action_1b, :limit => 30
      t.string :name1_prov5_action_1b, :limit => 30
      t.string :name1_prov1_action_1c, :limit => 30
      t.string :name1_prov2_action_1c, :limit => 30
      t.string :name1_prov3_action_1c, :limit => 30
      t.string :name1_prov4_action_1c, :limit => 30
      t.string :name1_prov5_action_1c, :limit => 30
      t.string :name1_prov1_action_2a, :limit => 30
      t.string :name1_prov2_action_2a, :limit => 30
      t.string :name1_prov3_action_2a, :limit => 30
      t.string :name1_prov4_action_2a, :limit => 30
      t.string :name1_prov5_action_2a, :limit => 30
      t.string :name1_prov1_action_2b, :limit => 30
      t.string :name1_prov2_action_2b, :limit => 30
      t.string :name1_prov3_action_2b, :limit => 30
      t.string :name1_prov4_action_2b, :limit => 30
      t.string :name1_prov5_action_2b, :limit => 30
      t.string :name1_prov1_action_2c, :limit => 30
      t.string :name1_prov2_action_2c, :limit => 30
      t.string :name1_prov3_action_2c, :limit => 30
      t.string :name1_prov4_action_2c, :limit => 30
      t.string :name1_prov5_action_2c, :limit => 30
      t.string :name1_prov1_action_3a, :limit => 30
      t.string :name1_prov2_action_3a, :limit => 30
      t.string :name1_prov3_action_3a, :limit => 30
      t.string :name1_prov4_action_3a, :limit => 30
      t.string :name1_prov5_action_3a, :limit => 30
      t.string :name1_prov1_action_3b, :limit => 30
      t.string :name1_prov2_action_3b, :limit => 30
      t.string :name1_prov3_action_3b, :limit => 30
      t.string :name1_prov4_action_3b, :limit => 30
      t.string :name1_prov5_action_3b, :limit => 30
      t.string :name1_prov1_action_3c, :limit => 30
      t.string :name1_prov2_action_3c, :limit => 30
      t.string :name1_prov3_action_3c, :limit => 30
      t.string :name1_prov4_action_3c, :limit => 30
      t.string :name1_prov5_action_3c, :limit => 30

      t.integer :cost1_action1_1a
      t.integer :cost1_action2_1a
      t.integer :cost1_action3_1a
      t.integer :cost1_action4_1a
      t.integer :cost1_action5_1a
      t.integer :cost1_action1_1b
      t.integer :cost1_action2_1b
      t.integer :cost1_action3_1b
      t.integer :cost1_action4_1b
      t.integer :cost1_action5_1b
      t.integer :cost1_action1_1c
      t.integer :cost1_action2_1c
      t.integer :cost1_action3_1c
      t.integer :cost1_action4_1c
      t.integer :cost1_action5_1c
      t.integer :cost1_action1_2a
      t.integer :cost1_action2_2a
      t.integer :cost1_action3_2a
      t.integer :cost1_action4_2a
      t.integer :cost1_action5_2a
      t.integer :cost1_action1_2b
      t.integer :cost1_action2_2b
      t.integer :cost1_action3_2b
      t.integer :cost1_action4_2b
      t.integer :cost1_action5_2b
      t.integer :cost1_action1_2c
      t.integer :cost1_action2_2c
      t.integer :cost1_action3_2c
      t.integer :cost1_action4_2c
      t.integer :cost1_action5_2c
      t.integer :cost1_action1_3a
      t.integer :cost1_action2_3a
      t.integer :cost1_action3_3a
      t.integer :cost1_action4_3a
      t.integer :cost1_action5_3a
      t.integer :cost1_action1_3b
      t.integer :cost1_action2_3b
      t.integer :cost1_action3_3b
      t.integer :cost1_action4_3b
      t.integer :cost1_action5_3b
      t.integer :cost1_action1_3c
      t.integer :cost1_action2_3c
      t.integer :cost1_action3_3c
      t.integer :cost1_action4_3c
      t.integer :cost1_action5_3c

      t.string :name2_prov1_action_1a, :limit => 30
      t.string :name2_prov2_action_1a, :limit => 30
      t.string :name2_prov3_action_1a, :limit => 30
      t.string :name2_prov4_action_1a, :limit => 30
      t.string :name2_prov5_action_1a, :limit => 30
      t.string :name2_prov1_action_1b, :limit => 30
      t.string :name2_prov2_action_1b, :limit => 30
      t.string :name2_prov3_action_1b, :limit => 30
      t.string :name2_prov4_action_1b, :limit => 30
      t.string :name2_prov5_action_1b, :limit => 30
      t.string :name2_prov1_action_1c, :limit => 30
      t.string :name2_prov2_action_1c, :limit => 30
      t.string :name2_prov3_action_1c, :limit => 30
      t.string :name2_prov4_action_1c, :limit => 30
      t.string :name2_prov5_action_1c, :limit => 30
      t.string :name2_prov1_action_2a, :limit => 30
      t.string :name2_prov2_action_2a, :limit => 30
      t.string :name2_prov3_action_2a, :limit => 30
      t.string :name2_prov4_action_2a, :limit => 30
      t.string :name2_prov5_action_2a, :limit => 30
      t.string :name2_prov1_action_2b, :limit => 30
      t.string :name2_prov2_action_2b, :limit => 30
      t.string :name2_prov3_action_2b, :limit => 30
      t.string :name2_prov4_action_2b, :limit => 30
      t.string :name2_prov5_action_2b, :limit => 30
      t.string :name2_prov1_action_2c, :limit => 30
      t.string :name2_prov2_action_2c, :limit => 30
      t.string :name2_prov3_action_2c, :limit => 30
      t.string :name2_prov4_action_2c, :limit => 30
      t.string :name2_prov5_action_2c, :limit => 30
      t.string :name2_prov1_action_3a, :limit => 30
      t.string :name2_prov2_action_3a, :limit => 30
      t.string :name2_prov3_action_3a, :limit => 30
      t.string :name2_prov4_action_3a, :limit => 30
      t.string :name2_prov5_action_3a, :limit => 30
      t.string :name2_prov1_action_3b, :limit => 30
      t.string :name2_prov2_action_3b, :limit => 30
      t.string :name2_prov3_action_3b, :limit => 30
      t.string :name2_prov4_action_3b, :limit => 30
      t.string :name2_prov5_action_3b, :limit => 30
      t.string :name2_prov1_action_3c, :limit => 30
      t.string :name2_prov2_action_3c, :limit => 30
      t.string :name2_prov3_action_3c, :limit => 30
      t.string :name2_prov4_action_3c, :limit => 30
      t.string :name2_prov5_action_3c, :limit => 30

      t.integer :cost2_action1_1a
      t.integer :cost2_action2_1a
      t.integer :cost2_action3_1a
      t.integer :cost2_action4_1a
      t.integer :cost2_action5_1a
      t.integer :cost2_action1_1b
      t.integer :cost2_action2_1b
      t.integer :cost2_action3_1b
      t.integer :cost2_action4_1b
      t.integer :cost2_action5_1b
      t.integer :cost2_action1_1c
      t.integer :cost2_action2_1c
      t.integer :cost2_action3_1c
      t.integer :cost2_action4_1c
      t.integer :cost2_action5_1c
      t.integer :cost2_action1_2a
      t.integer :cost2_action2_2a
      t.integer :cost2_action3_2a
      t.integer :cost2_action4_2a
      t.integer :cost2_action5_2a
      t.integer :cost2_action1_2b
      t.integer :cost2_action2_2b
      t.integer :cost2_action3_2b
      t.integer :cost2_action4_2b
      t.integer :cost2_action5_2b
      t.integer :cost2_action1_2c
      t.integer :cost2_action2_2c
      t.integer :cost2_action3_2c
      t.integer :cost2_action4_2c
      t.integer :cost2_action5_2c
      t.integer :cost2_action1_3a
      t.integer :cost2_action2_3a
      t.integer :cost2_action3_3a
      t.integer :cost2_action4_3a
      t.integer :cost2_action5_3a
      t.integer :cost2_action1_3b
      t.integer :cost2_action2_3b
      t.integer :cost2_action3_3b
      t.integer :cost2_action4_3b
      t.integer :cost2_action5_3b
      t.integer :cost2_action1_3c
      t.integer :cost2_action2_3c
      t.integer :cost2_action3_3c
      t.integer :cost2_action4_3c
      t.integer :cost2_action5_3c

      t.string :name3_prov1_action_1a, :limit => 30
      t.string :name3_prov2_action_1a, :limit => 30
      t.string :name3_prov3_action_1a, :limit => 30
      t.string :name3_prov4_action_1a, :limit => 30
      t.string :name3_prov5_action_1a, :limit => 30
      t.string :name3_prov1_action_1b, :limit => 30
      t.string :name3_prov2_action_1b, :limit => 30
      t.string :name3_prov3_action_1b, :limit => 30
      t.string :name3_prov4_action_1b, :limit => 30
      t.string :name3_prov5_action_1b, :limit => 30
      t.string :name3_prov1_action_1c, :limit => 30
      t.string :name3_prov2_action_1c, :limit => 30
      t.string :name3_prov3_action_1c, :limit => 30
      t.string :name3_prov4_action_1c, :limit => 30
      t.string :name3_prov5_action_1c, :limit => 30
      t.string :name3_prov1_action_2a, :limit => 30
      t.string :name3_prov2_action_2a, :limit => 30
      t.string :name3_prov3_action_2a, :limit => 30
      t.string :name3_prov4_action_2a, :limit => 30
      t.string :name3_prov5_action_2a, :limit => 30
      t.string :name3_prov1_action_2b, :limit => 30
      t.string :name3_prov2_action_2b, :limit => 30
      t.string :name3_prov3_action_2b, :limit => 30
      t.string :name3_prov4_action_2b, :limit => 30
      t.string :name3_prov5_action_2b, :limit => 30
      t.string :name3_prov1_action_2c, :limit => 30
      t.string :name3_prov2_action_2c, :limit => 30
      t.string :name3_prov3_action_2c, :limit => 30
      t.string :name3_prov4_action_2c, :limit => 30
      t.string :name3_prov5_action_2c, :limit => 30
      t.string :name3_prov1_action_3a, :limit => 30
      t.string :name3_prov2_action_3a, :limit => 30
      t.string :name3_prov3_action_3a, :limit => 30
      t.string :name3_prov4_action_3a, :limit => 30
      t.string :name3_prov5_action_3a, :limit => 30
      t.string :name3_prov1_action_3b, :limit => 30
      t.string :name3_prov2_action_3b, :limit => 30
      t.string :name3_prov3_action_3b, :limit => 30
      t.string :name3_prov4_action_3b, :limit => 30
      t.string :name3_prov5_action_3b, :limit => 30
      t.string :name3_prov1_action_3c, :limit => 30
      t.string :name3_prov2_action_3c, :limit => 30
      t.string :name3_prov3_action_3c, :limit => 30
      t.string :name3_prov4_action_3c, :limit => 30
      t.string :name3_prov5_action_3c, :limit => 30

      t.integer :cost3_action1_1a
      t.integer :cost3_action2_1a
      t.integer :cost3_action3_1a
      t.integer :cost3_action4_1a
      t.integer :cost3_action5_1a
      t.integer :cost3_action1_1b
      t.integer :cost3_action2_1b
      t.integer :cost3_action3_1b
      t.integer :cost3_action4_1b
      t.integer :cost3_action5_1b
      t.integer :cost3_action1_1c
      t.integer :cost3_action2_1c
      t.integer :cost3_action3_1c
      t.integer :cost3_action4_1c
      t.integer :cost3_action5_1c
      t.integer :cost3_action1_2a
      t.integer :cost3_action2_2a
      t.integer :cost3_action3_2a
      t.integer :cost3_action4_2a
      t.integer :cost3_action5_2a
      t.integer :cost3_action1_2b
      t.integer :cost3_action2_2b
      t.integer :cost3_action3_2b
      t.integer :cost3_action4_2b
      t.integer :cost3_action5_2b
      t.integer :cost3_action1_2c
      t.integer :cost3_action2_2c
      t.integer :cost3_action3_2c
      t.integer :cost3_action4_2c
      t.integer :cost3_action5_2c
      t.integer :cost3_action1_3a
      t.integer :cost3_action2_3a
      t.integer :cost3_action3_3a
      t.integer :cost3_action4_3a
      t.integer :cost3_action5_3a
      t.integer :cost3_action1_3b
      t.integer :cost3_action2_3b
      t.integer :cost3_action3_3b
      t.integer :cost3_action4_3b
      t.integer :cost3_action5_3b
      t.integer :cost3_action1_3c
      t.integer :cost3_action2_3c
      t.integer :cost3_action3_3c
      t.integer :cost3_action4_3c
      t.integer :cost3_action5_3c

      t.string :selection_prov1_action_1a, :limit => 30
      t.string :selection_prov2_action_1a, :limit => 30
      t.string :selection_prov3_action_1a, :limit => 30
      t.string :selection_prov4_action_1a, :limit => 30
      t.string :selection_prov5_action_1a, :limit => 30
      t.string :selection_prov1_action_1b, :limit => 30
      t.string :selection_prov2_action_1b, :limit => 30
      t.string :selection_prov3_action_1b, :limit => 30
      t.string :selection_prov4_action_1b, :limit => 30
      t.string :selection_prov5_action_1b, :limit => 30
      t.string :selection_prov1_action_1c, :limit => 30
      t.string :selection_prov2_action_1c, :limit => 30
      t.string :selection_prov3_action_1c, :limit => 30
      t.string :selection_prov4_action_1c, :limit => 30
      t.string :selection_prov5_action_1c, :limit => 30
      t.string :selection_prov1_action_2a, :limit => 30
      t.string :selection_prov2_action_2a, :limit => 30
      t.string :selection_prov3_action_2a, :limit => 30
      t.string :selection_prov4_action_2a, :limit => 30
      t.string :selection_prov5_action_2a, :limit => 30
      t.string :selection_prov1_action_2b, :limit => 30
      t.string :selection_prov2_action_2b, :limit => 30
      t.string :selection_prov3_action_2b, :limit => 30
      t.string :selection_prov4_action_2b, :limit => 30
      t.string :selection_prov5_action_2b, :limit => 30
      t.string :selection_prov1_action_2c, :limit => 30
      t.string :selection_prov2_action_2c, :limit => 30
      t.string :selection_prov3_action_2c, :limit => 30
      t.string :selection_prov4_action_2c, :limit => 30
      t.string :selection_prov5_action_2c, :limit => 30
      t.string :selection_prov1_action_3a, :limit => 30
      t.string :selection_prov2_action_3a, :limit => 30
      t.string :selection_prov3_action_3a, :limit => 30
      t.string :selection_prov4_action_3a, :limit => 30
      t.string :selection_prov5_action_3a, :limit => 30
      t.string :selection_prov1_action_3b, :limit => 30
      t.string :selection_prov2_action_3b, :limit => 30
      t.string :selection_prov3_action_3b, :limit => 30
      t.string :selection_prov4_action_3b, :limit => 30
      t.string :selection_prov5_action_3b, :limit => 30
      t.string :selection_prov1_action_3c, :limit => 30
      t.string :selection_prov2_action_3c, :limit => 30
      t.string :selection_prov3_action_3c, :limit => 30
      t.string :selection_prov4_action_3c, :limit => 30
      t.string :selection_prov5_action_3c, :limit => 30

      t.integer :selection_cost_action1_1a
      t.integer :selection_cost_action2_1a
      t.integer :selection_cost_action3_1a
      t.integer :selection_cost_action4_1a
      t.integer :selection_cost_action5_1a
      t.integer :selection_cost_action1_1b
      t.integer :selection_cost_action2_1b
      t.integer :selection_cost_action3_1b
      t.integer :selection_cost_action4_1b
      t.integer :selection_cost_action5_1b
      t.integer :selection_cost_action1_1c
      t.integer :selection_cost_action2_1c
      t.integer :selection_cost_action3_1c
      t.integer :selection_cost_action4_1c
      t.integer :selection_cost_action5_1c
      t.integer :selection_cost_action1_2a
      t.integer :selection_cost_action2_2a
      t.integer :selection_cost_action3_2a
      t.integer :selection_cost_action4_2a
      t.integer :selection_cost_action5_2a
      t.integer :selection_cost_action1_2b
      t.integer :selection_cost_action2_2b
      t.integer :selection_cost_action3_2b
      t.integer :selection_cost_action4_2b
      t.integer :selection_cost_action5_2b
      t.integer :selection_cost_action1_2c
      t.integer :selection_cost_action2_2c
      t.integer :selection_cost_action3_2c
      t.integer :selection_cost_action4_2c
      t.integer :selection_cost_action5_2c
      t.integer :selection_cost_action1_3a
      t.integer :selection_cost_action2_3a
      t.integer :selection_cost_action3_3a
      t.integer :selection_cost_action4_3a
      t.integer :selection_cost_action5_3a
      t.integer :selection_cost_action1_3b
      t.integer :selection_cost_action2_3b
      t.integer :selection_cost_action3_3b
      t.integer :selection_cost_action4_3b
      t.integer :selection_cost_action5_3b
      t.integer :selection_cost_action1_3c
      t.integer :selection_cost_action2_3c
      t.integer :selection_cost_action3_3c
      t.integer :selection_cost_action4_3c
      t.integer :selection_cost_action5_3c

      t.integer :total_cost_action_1a
      t.integer :total_cost_action_1b
      t.integer :total_cost_action_1c
      t.integer :total_cost_action_2a
      t.integer :total_cost_action_2b
      t.integer :total_cost_action_2c
      t.integer :total_cost_action_3a
      t.integer :total_cost_action_3b
      t.integer :total_cost_action_3c

      t.timestamps null: false
    end
  end
end

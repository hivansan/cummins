class AddMethodologyProcessToProposals < ActiveRecord::Migration
  def change
    add_column :proposals, :methodology_process, :string
    add_column :proposals, :methodology_medium, :string
    add_column :proposals, :methodology_description, :string
    add_column :proposals, :impact_objective1, :string
    add_column :proposals, :impact_objective2, :string
    add_column :proposals, :impact_objective3, :string
    add_column :proposals, :impact_goal1, :string
    add_column :proposals, :impact_goal2, :string
    add_column :proposals, :impact_goal3, :string
    add_column :proposals, :impact_indicator1, :string
    add_column :proposals, :impact_indicator2, :string
    add_column :proposals, :impact_indicator3, :string
    add_column :proposals, :impact_evidence1, :string
    add_column :proposals, :impact_evidence2, :string
    add_column :proposals, :impact_evidence3, :string
  end
end

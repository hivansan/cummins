class ChangeLimits < ActiveRecord::Migration
  def change
    change_column :proposals,  :methodology_process, :string, limit: 1000
    change_column :proposals,  :methodology_medium, :string, limit: 1000
    change_column :proposals,  :methodology_description, :string, limit: 1000
    change_column :proposals,  :impact_objective1, :string, limit: 1000
    change_column :proposals,  :impact_objective2, :string, limit: 1000
    change_column :proposals,  :impact_objective3, :string, limit: 1000
    change_column :proposals,  :impact_goal1, :string, limit: 1000
    change_column :proposals,  :impact_goal2, :string, limit: 1000
    change_column :proposals,  :impact_goal3, :string, limit: 1000
    change_column :proposals,  :impact_indicator1, :string, limit: 1000
    change_column :proposals,  :impact_indicator2, :string, limit: 1000
    change_column :proposals,  :impact_indicator3, :string, limit: 1000
    change_column :proposals,  :impact_evidence1, :string, limit: 1000
    change_column :proposals,  :impact_evidence2, :string, limit: 1000
    change_column :proposals,  :impact_evidence3, :string, limit: 1000

    change_column :resources,  :name1_prov1_action_1a, :string, limit: 255
    change_column :resources,  :name1_prov2_action_1a, :string, limit: 255
    change_column :resources,  :name1_prov3_action_1a, :string, limit: 255
    change_column :resources,  :name1_prov4_action_1a, :string, limit: 255
    change_column :resources,  :name1_prov5_action_1a, :string, limit: 255
    change_column :resources,  :name1_prov1_action_1b, :string, limit: 255
    change_column :resources,  :name1_prov2_action_1b, :string, limit: 255
    change_column :resources,  :name1_prov3_action_1b, :string, limit: 255
    change_column :resources,  :name1_prov4_action_1b, :string, limit: 255
    change_column :resources,  :name1_prov5_action_1b, :string, limit: 255
    change_column :resources,  :name1_prov1_action_1c, :string, limit: 255
    change_column :resources,  :name1_prov2_action_1c, :string, limit: 255
    change_column :resources,  :name1_prov3_action_1c, :string, limit: 255
    change_column :resources,  :name1_prov4_action_1c, :string, limit: 255
    change_column :resources,  :name1_prov5_action_1c, :string, limit: 255
    change_column :resources,  :name1_prov1_action_2a, :string, limit: 255
    change_column :resources,  :name1_prov2_action_2a, :string, limit: 255
    change_column :resources,  :name1_prov3_action_2a, :string, limit: 255
    change_column :resources,  :name1_prov4_action_2a, :string, limit: 255
    change_column :resources,  :name1_prov5_action_2a, :string, limit: 255
    change_column :resources,  :name1_prov1_action_2b, :string, limit: 255
    change_column :resources,  :name1_prov2_action_2b, :string, limit: 255
    change_column :resources,  :name1_prov3_action_2b, :string, limit: 255
    change_column :resources,  :name1_prov4_action_2b, :string, limit: 255
    change_column :resources,  :name1_prov5_action_2b, :string, limit: 255
    change_column :resources,  :name1_prov1_action_2c, :string, limit: 255
    change_column :resources,  :name1_prov2_action_2c, :string, limit: 255
    change_column :resources,  :name1_prov3_action_2c, :string, limit: 255
    change_column :resources,  :name1_prov4_action_2c, :string, limit: 255
    change_column :resources,  :name1_prov5_action_2c, :string, limit: 255
    change_column :resources,  :name1_prov1_action_3a, :string, limit: 255
    change_column :resources,  :name1_prov2_action_3a, :string, limit: 255
    change_column :resources,  :name1_prov3_action_3a, :string, limit: 255
    change_column :resources,  :name1_prov4_action_3a, :string, limit: 255
    change_column :resources,  :name1_prov5_action_3a, :string, limit: 255
    change_column :resources,  :name1_prov1_action_3b, :string, limit: 255
    change_column :resources,  :name1_prov2_action_3b, :string, limit: 255
    change_column :resources,  :name1_prov3_action_3b, :string, limit: 255
    change_column :resources,  :name1_prov4_action_3b, :string, limit: 255
    change_column :resources,  :name1_prov5_action_3b, :string, limit: 255
    change_column :resources,  :name1_prov1_action_3c, :string, limit: 255
    change_column :resources,  :name1_prov2_action_3c, :string, limit: 255
    change_column :resources,  :name1_prov3_action_3c, :string, limit: 255
    change_column :resources,  :name1_prov4_action_3c, :string, limit: 255
    change_column :resources,  :name1_prov5_action_3c, :string, limit: 255
    
  end
end

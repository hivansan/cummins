class Limits < ActiveRecord::Migration
  def change
    change_column :proposals, :methodology_process, :string, :limit => 600
    change_column :proposals, :methodology_medium, :string, :limit => 600
    change_column :proposals, :methodology_description, :string, :limit => 600

    change_column :users, :mission_vision, :string, :limit => 800
  end
end

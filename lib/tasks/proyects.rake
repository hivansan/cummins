# encoding: UTF-8
namespace :proyects do
  desc 'dont leave blank spaces'
  task :update_proyects_attr => :environment do
    Proyect.all.each do |proyect| 
      proyect.importance_text = proyect.importance_text.nil? ? "" : proyect.importance_text
      proyect.importance_text_b = proyect.importance_text_b.nil? ? "" : proyect.importance_text_b
      proyect.importance_text_c = proyect.importance_text_c.nil? ? "" : proyect.importance_text_c
      proyect.importance_text_d = proyect.importance_text_d.nil? ? "" : proyect.importance_text_d
      proyect.importance_text_e = proyect.importance_text_e.nil? ? "" : proyect.importance_text_e
      proyect.importance_text_f = proyect.importance_text_f.nil? ? "" : proyect.importance_text_f
      proyect.importance_text_g = proyect.importance_text_g.nil? ? "" : proyect.importance_text_g
      proyect.importance_text_h = proyect.importance_text_h.nil? ? "" : proyect.importance_text_h

      proyect.improvement_importance_a = proyect.improvement_importance_a.nil? ? "" : proyect.improvement_importance_a
      proyect.improvement_importance_b = proyect.improvement_importance_b.nil? ? "" : proyect.improvement_importance_b
      proyect.improvement_importance_c = proyect.improvement_importance_c.nil? ? "" : proyect.improvement_importance_c
      proyect.improvement_importance_d = proyect.improvement_importance_d.nil? ? "" : proyect.improvement_importance_d
      proyect.improvement_importance_e = proyect.improvement_importance_e.nil? ? "" : proyect.improvement_importance_e
      proyect.improvement_importance_f = proyect.improvement_importance_f.nil? ? "" : proyect.improvement_importance_f
      proyect.save
    end
    #p "User: #{user.email} | Password: #{user.password}"
  end
end